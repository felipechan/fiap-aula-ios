//
//  ViewController.m
//  MeusDados
//
//  Created by Usuário Convidado on 11/02/19.
//  Copyright © 2019 Felipe. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    nome.text = @"Nome...";
    cidade.text = @"Cidade...";
    // Do any additional setup after loading the view, typically from a nib.
}


- (IBAction)exibir:(id)sender {
    NSLog(@"Boa Noite");
    nome.text = @"Felipe";
    cidade.text = @"São Paulo";
}

- (IBAction)limpar:(id)sender {
    nome.text = @"Nome...";
    cidade.text = @"Cidade...";
}

@end
